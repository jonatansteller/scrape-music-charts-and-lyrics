# SCRAPE CHECK
# Helps you check file names against header data to find out where LyricsGenius failed


# 1. Import libraries

print("\n1/3: Import libraries...", end=" ")

from os import listdir
from os.path import isfile, join

print("done.")


# 2. Find all files in the lyrics directory

print("2/3: Find all files in the lyrics directory...", end=" ")

files = [f for f in listdir("lyrics") if isfile(join("lyrics", f))]
files_counter = 0

print("done.")


# 3. Compile file names and headers into entries_check.txt

print("3/3: Compile file names and headers into entries_check.txt...", end=" ")

for file in files:

    # 3.1. Write file name and first two lines

    with open("entries_check.txt", "a") as f:
        f.write(file + "\n")
        with open("lyrics/" + file, "r") as check:
            f.write(check.readline() + check.readline() + "\n")

    # 3.2. Increase counter

    files_counter = files_counter + 1
    print(files_counter, "·", end=" ")

print("done.", end="\n\n")
print("File entries_check.txt compiled successfully.\nGood luck finding wrong entries!", end="\n\n")
