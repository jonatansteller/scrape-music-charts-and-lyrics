# SCRAPE CHARTS
# Scrapes a list of all songs in the British music charts and saves them as a JSON file


# 1. Import libraries

print("\n1/4: Import libraries...", end=" ")

from urllib.request import urlopen
from urllib.parse import unquote
from urllib.parse import quote
from bs4 import BeautifulSoup
from time import sleep
from json import dump

print("done.")


# 2. Compile source URLs of chart listings

print("2/4: Compile source URLs of chart listings...", end=" ")

sources_dates = [ "20201023", "20201030", "20201106" ] # TODO: ENTER THE CHART FRIDAYS YOU NEED HERE
sources = []

for sources_date in sources_dates:
    sources.append("https://www.officialcharts.com/charts/singles-chart/" + sources_date + "/7501/")

print("done.")


# 3. Look up and parse each URL

print("3/4: Look up and parse each URL...", end=" ")

sources_counter = 0
songs = []

for source in sources:
    html = urlopen(source)
    soup = BeautifulSoup(html, "html.parser")

    # 3.1. Find the chart entries

    section = soup.find("table", {"class": "chart-positions"})
    entries = section.find_all("div", {"class": "title-artist"})

    # 3.2. Save artist and title for each entry

    for entry in entries:
        entry_title = entry.find("div", {"class": "title"}).find("a")
        for child in entry_title.children:
            entry_title = child
        entry_artist = entry.find("div", {"class": "artist"}).find("a")
        for child in entry_artist.children:
            entry_artist = child

        # 3.2.1. Add a new entry if it is not in the array yet

        entry_filename = entry_artist + " - " + entry_title + ".txt"
        entry_array = [entry_artist, entry_title, entry_filename]
        if entry_array not in songs:
            songs.append(entry_array)

    # 3.3. Sleep for a second to be nice to the server (and avoid getting blocked)

    sources_counter = sources_counter + 1
    print(sources_counter, "·", end=" ")
    sleep(1)

print("done.")


# 4. Save array to entries.json

print("4/4: Save array to entries.json...", end=" ")

with open("entries.json", "w") as output:
    dump(songs, output, indent=4)

print("done.", end="\n\n")
print("List compiled successfully.\nYou may have to clean up artist features by hand.", end="\n\n")
