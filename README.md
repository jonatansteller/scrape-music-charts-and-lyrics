# Scrape Music Charts and Lyrics

Three Python scripts to scrape the British music charts and their corresponding song lyrics for academic purposes. Before you use these scripts, make sure that this type of data collection is legal in the country you are in and for the intended purpose.

- *Current version:* 0.8
- *Release date:* 25 March 2021
- *Dependencies:* Beautiful Soup 4, LyricsGenius

## To do

- To help genius.com find the right songs, the following manual find-and-replace expressions simplify the names of authors: replace ` FT .{1,}",` by `",` and `\/.{1,}",` by `",`. They should be added to `scrape_lyrics.py`.

## Setting up the Python environment

Go to your project folder that contains the scripts. In a (Linux) terminal, use the following commands to set up the two libraries the scripts need.

```python
python -m venv scrape
. scrape/bin/activate
python -m pip install bs4
python -m pip install lyricsgenius
```

## Setting up the scripts

Before you use `scrape_charts.py`, add the weeks you need the top 100 charts of to the line marked "todo." British music charts are published on Fridays, and the Charts Company uses the following date format: `20201106`.

Before you use the `scrape_lyrics.py` script, add a client access token or a user token in the "todo" line. For more information see the [authorisation section](https://lyricsgenius.readthedocs.io/en/master/setup.html#authorization) in the LyricsGenius documentation.

## Running the scripts

Before you start, enter the Python **environment** if it is not yet active by typing `. scrape/bin/activate`.

To run the **first script**, type `python scrape_charts.py`. It produces a file called `entries.json` in your project folder to use in the second script.

When the above file is ready, run the **second script** with `python scrape_lyrics.py`. If you are scraping a large number of songs, the genius.com server may stop responding at some point. If this is the case, just re-run the command until `entries.json` is empty. Lyrics are stored in the `lyrics` folder. When the script does not find a particular lyric, it stores the intended file name in `entries_notfound.txt` so it can be added manually.

When all lyrics are compiled, use the **third script** with `python scrape_check.py` to generate a file called `entries_check.txt`. It contains the file names and headers of all lyrics. If they do not match, LyricsGenius did not find the right page and the entry needs to be sorted out manually. Since there may occasionally be two spellings for the same song in chart listings, your final set of lyrics may contain duplicates that are usually easy to spot and remove (almost the same file name with the exact same file size).

To **leave** the Python environment when you are done, type `deactivate`.
