# SCRAPE LYRICS
# Scrapes song lyrics from genius.com based on a JSON list of songs


# 1. Import libraries

print("\n1/3: Import libraries...", end=" ")

from json import load
from json import dump
import lyricsgenius
genius = lyricsgenius.Genius("", timeout=10, sleep_time=1, verbose=False, skip_non_songs=True) # TODO: ENTER YOUR OWN KEY HERE

print("done.")


# 2. Load entries.json

print("2/3: Load entries.json...", end=" ")

with open("entries.json") as input:
    songs = load(input)
songs_counter = 0

print("done.")


# 3. Work through array to get the lyrics

print("3/3: Work through array to get the lyrics...", end=" ")

for entry in songs:

    # 3.1. Set variables

    entry_artist = entry[0]
    entry_title = entry[1]
    entry_filename = entry[2]

    # 3.2. Find the song in the genius.com database

    song = genius.search_song(entry_title, entry_artist)

    # 3.3. Download the lyrics

    if song:
        entry_filename = entry_filename.replace("/", " ")
        with open(f"lyrics/{entry_filename}", "w") as f:
            f.write(song.artist + "\n" + song.title + "\n\n" + song.lyrics)

    # 3.4. Write the entry to a backup file if the lyrics cannot be found

    else:
        with open("entries_notfound.txt", "a") as manual:
            manual.write(entry_filename + "\n")

    # 3.5. Remove the entry from an array copy and save it to entries.json

    songs_revised = songs.copy()
    songs_revised.remove(entry)
    with open("entries.json", "w") as output:
        dump(songs_revised, output, indent=4)
    songs_counter = songs_counter + 1
    print(songs_counter, "·", end=" ")

print("done.", end="\n\n")
print("Lyrics compiled successfully.\nCheck entries.json to see if you need to run the script again.", end="\n\n")
